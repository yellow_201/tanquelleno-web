/**
 * GasStationService.java
 * @author mimi
 * @date Feb 12, 2016
 */

package com.mob.tanquelleno;

import com.mob.mongo.DataBaseController;

import org.bson.Document;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

/**
 * @author mimi
 *
 */
@Path("gas")
public class GasStationService {
  @Context
  private UriInfo context;

  @GET
  @Produces(MediaType.TEXT_PLAIN)
  public String getText() {
    return "this is GasStations";// TODO return proper representation object
    // throw new UnsupportedOperationException();
  }

  /**
   * @param lat
   * @param lon
   * @return
   */
  @GET
  @Path("/gas/{lat}/{lon}")
  @Consumes(MediaType.TEXT_HTML)
  @Produces(MediaType.APPLICATION_JSON)
  public Response near(@PathParam("lat") Double lat, @PathParam("lon") Double lon) {

    List<Document> gas = DataBaseController.getInstance().findGasStationNear(lon, lat);
    if (gas != null) {
      return Response.ok(gas.get(0).toJson(), MediaType.APPLICATION_JSON).build();
    }
    return Response.serverError().build();
  }
}
