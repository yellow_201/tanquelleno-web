package com.mob.tanquelleno;


import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriInfo;

/**
 * @author yo
 *
 */
@Path("sayhello")
public class HelloWorld {
  @Context
  private UriInfo context;

  @GET
  @Produces(MediaType.TEXT_PLAIN)
  public String getText() {
    return "Hola todo el mundo!!!";// TODO return proper representation object
    // throw new UnsupportedOperationException();
  }

 
}
