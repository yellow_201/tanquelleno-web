package com.mob.tanquelleno;

import com.mob.mongo.DataBaseController;

import java.util.Date;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;



/**
 * REST Web Service
 *
 * @author Luis
 */
@Path("usershandler")
public class UsersHandlerService {

	@Context
    private UriInfo context;

	@GET
	@Path("/create/{user}/{password}/{email}")
	@Consumes(MediaType.TEXT_HTML)
	@Produces(MediaType.TEXT_HTML)
	public Response createUser(@PathParam("user") String userName,
      @PathParam("password") String password,
      @PathParam("email") String email) {

		DataBaseController.getInstance().addUser(userName,password, email, new Date());

		return null;
		//Response.status(Response.Status.UNAUTHORIZED).build();
	}

	/**
     * Retrieves representation of an instance of com.mob.tanquelleno.GenericResource
     * @return an instance of java.lang.String
     */
    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String getText() {
        return "this is generic";//TODO return proper representation object
//        throw new UnsupportedOperationException();
    }

    /**
     * PUT method for updating or creating an instance of GenericResource
     * @param content representation for the resource
     */
    @PUT
    @Consumes(MediaType.TEXT_PLAIN)
    public void putText(String content) {
    }

}
