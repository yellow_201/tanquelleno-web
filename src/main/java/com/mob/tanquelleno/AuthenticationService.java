package com.mob.tanquelleno;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
//import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import com.mob.authentication.AuthenticationController;
import com.mob.authentication.Credentials;
import com.mob.mongo.DataBaseController;

/**
 * REST Web Service
 *
 * @author Luis
 */
@Path("auth")
public class AuthenticationService {

	@Context
    private UriInfo context;
	private AuthenticationController authController;

	@GET//TODO @POST
	@Path("/authenticate/{user}/{password}")
	@Consumes(MediaType.TEXT_HTML)
	@Produces(MediaType.TEXT_HTML)
	public Response authenticateUser(@PathParam("user") String userName,
	                   @PathParam("password") String password) {

		Credentials user = new Credentials(userName, password);
//  TODO validate user against Database
//  if (DataBaseController.getInstance().validateUser(userName, password)){
		authController = AuthenticationController.getInstance();
		String token = authController.createToken(user);

		return Response.ok(token).build();
//  }
//  else
		//Response.status(Response.Status.UNAUTHORIZED).build();
	}



	 /**
     * Retrieves representation of an instance of com.mob.tanquelleno.GenericResource
     * @return an instance of java.lang.String
     */
    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String getText() {
        return "this is generic";//TODO return proper representation object
//        throw new UnsupportedOperationException();
    }

    /**
     * PUT method for updating or creating an instance of GenericResource
     * @param content representation for the resource
     */
    @PUT
    @Consumes(MediaType.TEXT_PLAIN)
    public void putText(String content) {
    }
}
