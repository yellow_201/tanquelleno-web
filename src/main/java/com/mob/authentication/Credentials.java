package com.mob.authentication;

import java.io.Serializable;

/*
 * User credentials class containing the basic info to create a session
 * 
 */
public class Credentials implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8064719955091182809L;
	private String userName;
	private String password;
	
	public Credentials(String userName, String password){
		this.setUserName(userName);
		this.setPassword(password);
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
}
