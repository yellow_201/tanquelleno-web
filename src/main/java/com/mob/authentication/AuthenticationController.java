package com.mob.authentication;


import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
//import io.jsonwebtoken.impl.crypto.MacProvider;
//import java.security.Key;
import java.util.Date;


public class AuthenticationController {
	
	private static AuthenticationController instance = null;
	private final String AUTH_KEY = "dhjeyuehdty";
	
	public static AuthenticationController getInstance() {
	      if(instance == null) {
	         instance = new AuthenticationController();
	      }
	      return instance;
	   }
	
	private AuthenticationController(){}
	
	/*
	 * Creates authentication token for an specific user
	 * 
	 */
    public String createToken(Credentials credentials) {	
		String token = Jwts.builder().setSubject(credentials.getUserName()).setIssuedAt(new Date()).
				signWith(SignatureAlgorithm.HS256, AUTH_KEY).compact();
			
		return token;
    }
    
    public String getString(){
    	return "Esto es una prueba";
    }
    
    
}
