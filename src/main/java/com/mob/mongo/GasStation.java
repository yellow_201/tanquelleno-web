/**
 * GasStation.java
 * @author mimi
 * @date Feb 8, 2016
 */

package com.mob.mongo;

import static com.mongodb.client.model.Filters.eq;
import static com.mongodb.client.model.Filters.nearSphere;

import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;
import org.bson.conversions.Bson;

import java.util.ArrayList;
import java.util.List;

/**
 * @author mimi
 *
 */
public class GasStation {
  private final MongoCollection<Document> gasCollection;

  GasStation(final MongoDatabase database) {
    gasCollection = database.getCollection("Gasolinera");
  }

  /**
   * @param lon
   *          Longitude
   * @param lat
   *          Latitude
   * @param dist
   *          <i>optional</i> limit the $near results by <b>max</b> distance in meters
   * @return a list of gas stations near the given (current) location
   */
  public List<Document> findGasStationNear(Double lon, Double lat, Double... dist) {

    Bson near = nearSphere("loc", (double) lon, (double) lat, dist.length > 0 ? dist[0] : null,
        null);
    List<Document> gasStations = gasCollection.find(near).into(new ArrayList<Document>());

    return gasStations;

  }

  public Document findGasStationbyName(String name) {

    return gasCollection.find(eq("RazonSocial", name)).first();
  }

}
