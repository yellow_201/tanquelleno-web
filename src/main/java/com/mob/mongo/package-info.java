/**
 * Data access components for persisting data to the Mongo DB.
 *
 * <p>The Data access objects are only accessible within the package.
 *
 * @author mimi
 *
 */
package com.mob.mongo;