/**
 * User.java
 * This handles DB operations for the Users collection.
 * @author mimi
 * @date Feb 8, 2016
 */
package com.mob.mongo;

import static com.mongodb.client.model.Filters.eq;

import com.mongodb.ErrorCategory;
import com.mongodb.MongoWriteException;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;
import sun.misc.BASE64Encoder;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Date;
import java.util.Random;

/**
 * @author mimi
 *
 */
class User {
  private final MongoCollection<Document> usersCollection;
  private Random random = new SecureRandom();

  public User(final MongoDatabase database) {
    usersCollection = database.getCollection("users");
  }

  /**
   * validates that username is unique and insert into db.
   *
   * @param username
   * @param password
   * @param email
   * @param registrationDate
   * @return
   */
  public boolean addUser(String username, String password, String email, Date registrationDate) {

    String passwordHash = makePasswordHash(password, Integer.toString(random.nextInt()));

    Document user = new Document();

    user.append("_id", username).append("password", passwordHash).append("email", email)
        .append("registrationDate", registrationDate);

    try {
      usersCollection.insertOne(user);
      return true;
    } catch (MongoWriteException e) {
      if (e.getError().getCategory().equals(ErrorCategory.DUPLICATE_KEY)) {
        System.out.println("Username already in use: " + username);
        return false;
      }
      throw e;
    }
  }

  private String makePasswordHash(String password, String salt) {
    // @TODO
    try {
      String saltedAndHashed = password + "," + salt;
      MessageDigest digest = MessageDigest.getInstance("MD5");
      digest.update(saltedAndHashed.getBytes());

      BASE64Encoder encoder = new BASE64Encoder();
      byte[] hashedBytes = (new String(digest.digest(), "UTF-8")).getBytes();

      return encoder.encode(hashedBytes) + "," + salt;
    }

    catch (NoSuchAlgorithmException e) {
      throw new RuntimeException("MD5 is not available", e);
    } catch (UnsupportedEncodingException e) {
      throw new RuntimeException("UTF-8 unavailable?  Not a chance", e);
    }
  }

  public Document validateLogin(String username, String password) {
    Document user;

    user = usersCollection.find(eq("_id", username)).first();

    if (user == null) {
      System.out.println("User not in database");
      return null;
    }

    String hashedAndSalted = user.get("password").toString();

    String salt = hashedAndSalted.split(",")[1];

    if (!hashedAndSalted.equals(makePasswordHash(password, salt))) {
      System.out.println("Submitted password is not a match");
      return null;
    }

    return user;
  }
}
