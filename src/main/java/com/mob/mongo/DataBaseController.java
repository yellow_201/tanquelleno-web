package com.mob.mongo;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.MongoException;
import com.mongodb.client.MongoDatabase;

import org.bson.Document;

import java.util.Date;
import java.util.List;

/*
 * Basic DB controller
 * This class should handle all DB connections/queries
 *
 */
public class DataBaseController {

  private static DataBaseController instance;
  private final String DB_NAME = "tanque";
  private final String MONGO_URI = "mongodb://localhost:27017";
  MongoDatabase database = null;
  private GasStation gasStationDAO;
  private User userDAO;

  private DataBaseController() {
    try {
      MongoClient mongoClient = new MongoClient(new MongoClientURI(MONGO_URI));
      database = mongoClient.getDatabase(DB_NAME);

      gasStationDAO = new GasStation(database);
      userDAO = new User(database);
    } catch (MongoException e) {
      e.printStackTrace();
    }
  }

  public static synchronized DataBaseController getInstance() {
    if (instance == null) {
      instance = new DataBaseController();
    }
    return instance;
  }

  public boolean validateUser(String username, String password) {
    if (userDAO != null) {
      return userDAO.validateLogin(username, password) != null;
    }
    return false;
  }

  public boolean addUser(String username, String password, String email, Date registrationDate) {
    if (userDAO != null) {
      userDAO.addUser(username, password, email, registrationDate);
    }
    return false;

  }

  public List<Document> findGasStationNear(Double lon, Double lat, Double... dist) {
    if (gasStationDAO != null) {
      return gasStationDAO.findGasStationNear(lon, lat, dist);
    }
    return null;

  }
}
