package com.mob.user;

import java.util.Date;

public class User {

	private String userName;
	private String password;
	private String id;
	private String mail;
	private Date registrationDate;
	
	public User(String userName, String password, String id, String mail, Date registrationDate){
		this.setUserName(userName);
		this.setPassword(password);
		this.setId(id);
		this.setMail(mail);
		this.setRegistrationDate(registrationDate);
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public Date getRegistrationDate() {
		return registrationDate;
	}

	public void setRegistrationDate(Date registrationDate) {
		this.registrationDate = registrationDate;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
	
}
