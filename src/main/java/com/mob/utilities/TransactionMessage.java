package com.mob.utilities;

/*
 * ALL POSSIBLE ERROR/SUCCESS MESSAGES
 */
public enum TransactionMessage {

	USER_ADDED_SUCCESFULLY("The user was added to the DB", true),
	
	USER_NOT_ADDED("The user was NOT added to the DB", true),
	
    USER_UPDATED_SUCCESFULLY("The user was updated", true),
	
	USER_NOT_UPDATED("The user was NOT updated", true),
	
	USER_REMOVED_SUCCESFULLY("The user was removed from the DB", true),
	
	USER_NOT_REMOVED("The user was NOT removed from the DB", false),
	
	USER_ALREADY_EXISTS("The user name is already in use", false),
	
	INVALID_USER_NAME("User name not valid", false),
	
	INVALID_PASSWORD("Password not valid", false);
    
    private final String  description;
    private final boolean success;

    TransactionMessage(String description, boolean success) {
        this.description = description;
        this.success = success;
    }

	public String getDescription() {
		return description;
	}

	public boolean isSuccess() {
		return success;
	}
	
}
